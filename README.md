### FTP Keylogger ###

Purpose is to demonstrate usage and knowledge of windows API, winsock, and SMTP email protocol, and telnet.
Using mandrill transactional email service to send emails.

Keylogger that records keyboard strokes and sends to a hardcoded email, and also uploads to FTP remote server which is also hardcoded. 
There is no user interface for this project, the code here is only for the keylogger itself, not the generator, or user specifications.

User would control the keylogger by setting the keylogger to a specific ftp server and placing a file called "command.txt" with the following content

Command.txt:

<COMMAND HERE>
<SLEEP TIME>
<EMAIL TO SEND TO>
*

This formatting is as such so that the program reads and follows instructions by making reads to command.txt and parsing its 3 tokens line by line.
The * delimits the end of file. The commands are as follows: CONTINUE, PAUSE, KILL

Continue means to keep keylogger logging.
Pause means to temporarily stop keylogger.
Kill means that the keylogger takes itself out of startup, and deletes its own file via MoveFileEx.

Once the user clicks the keylogger.exe file, it will call the "Stealth" function to hide the console, and make itself invisible to the user. Then it will add itself to windows startup.
Afterwards, keylogger begins a new thread with keyboardhook to record keystrokes. At periodic times, keylogger will read command.txt and parse and follow the commands given. (Switch statement in main.cpp)


